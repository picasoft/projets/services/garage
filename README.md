# Garage

Service [garage](https://garagehq.deuxfleurs.fr) utilisé pour faire du stockage de données réparties.

## Lancement

Après avoir vérifier que les bons dossiers sont créés (ici `/DATA/garage/rhz_data` et `/DATA/garage/rhz_meta`), il suffit de `git pull` et `docker compose up -d` pour lancer le service.

Plus de doc est présente sur [le wiki](https://wiki.picasoft.net/doku.php?id=technique:adminsys:backup:garage).
